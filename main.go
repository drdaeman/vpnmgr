package main

import (
	"crypto/rand"
	"errors"
	"fmt"
	"html/template"
	"log"
	"net"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/csrf"
	"github.com/gorilla/mux"
	"gopkg.in/alecthomas/kingpin.v2"
)

var t = template.Must(template.New("index.tmpl").Parse(`<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Choose your destiny</title>
    <link rel="stylesheet" type="text/css" href="/assets/style.css" />
</head>
<body>
    <form class="wrapper" action="/select" method="POST">
		{{ .csrfField }}
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <button type="submit"{{if eq .active "us" }} class="active"{{end}} name="use" value="us">
                        <img src="/assets/us.png" alt="US" />
                    </button>
                </div>
                <div class="col text-center">
                    <button type="submit"{{if eq .active "nl" }} class="active"{{end}} name="use" value="nl">
                        <img src="/assets/nl.png" alt="NL" />
                    </button>
                </div>
            </div>
        </div>
    </form>
</body>
</html>`))

var db *ConnectedUsers

func requestIP(r *http.Request) (net.IP, error) {
	ip, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		return nil, err
	}

	userIP := net.ParseIP(ip)
	if userIP == nil {
		return nil, errors.New("Failed to parse IP")
	}
	return userIP, nil
}

func homeHandler(w http.ResponseWriter, r *http.Request) {
	ip, err := requestIP(r)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte("500 internal server error"))
		return
	}
	conn, err := db.Get(ip)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte("500 internal server error"))
		return
	}
	region := "us"
	if conn != nil {
		region = conn.Region
	}

	w.Header().Add("Content-Type", "text/html; charset=utf-8")
	w.WriteHeader(200)
	t.ExecuteTemplate(w, "index.tmpl", map[string]interface{}{
		csrf.TemplateTag: csrf.TemplateField(r),
		"active":         region,
	})
}

func selectHandler(w http.ResponseWriter, r *http.Request) {
	ip, err := requestIP(r)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte("500 internal server error"))
		return
	}
	conn, err := db.Get(ip)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte("500 internal server error"))
		return
	}

	if conn != nil {
		r.ParseForm()
		use := r.Form["use"]
		if len(use) == 1 {
			log.Printf("Setting '%s' to use '%s'", conn.Username, use[0])
			db.Set(conn.Username, use[0])
		}
	}

	w.Header().Add("Content-Type", "text/plain; charset=utf-8")
	w.Header().Add("Location", "/")
	w.WriteHeader(303)
	w.Write([]byte("303 see other"))
}

func main() {
	var err error

	app := kingpin.New("vpnmgr", "VPN management tool")
	database := app.Flag("db", "Database file path").Default("vpnmgr.db").String()

	serve := app.Command("run", "Starts a server")
	address := serve.Flag("addr", "Address to listen on").Default("127.0.0.1:8000").String()
	secureCookie := serve.Flag("secure-cookie", "Use secure cookies").Bool()

	add := app.Command("add", "Registers user connection")
	addIP := add.Arg("ip", "IP address").Required().IP()
	addUser := add.Arg("user", "User identifier").Required().String()

	remove := app.Command("remove", "Unregisters a connection")
	removeIP := remove.Arg("ip", "IP address").Required().IP()

	list := app.Command("list", "List registered connections")

	get := app.Command("get", "Get connection's region preference")
	getIP := get.Arg("ip", "IP address").Required().IP()

	cmd := kingpin.MustParse(app.Parse(os.Args[1:]))
	db, err = OpenDatabase(*database)
	if err != nil {
		log.Fatalf("Failed to open database: %s", err)
	}

	switch cmd {
	case add.FullCommand():
		err = db.Add(*addIP, *addUser)
		if err != nil {
			log.Fatal(err)
		}
	case remove.FullCommand():
		err = db.Remove(*removeIP)
		if err != nil {
			log.Fatal(err)
		}
	case list.FullCommand():
		ips, err := db.GetIPs()
		if err != nil {
			log.Fatal(err)
		}
		for ip, conn := range ips {
			fmt.Printf("%s\t%s\t%s\n", ip, conn.Username, conn.Region)
		}
	case get.FullCommand():
		conn, err := db.Get(*getIP)
		if err == nil && conn != nil {
			fmt.Println(conn.Region)
		} else {
			fmt.Println("us")
		}
	case serve.FullCommand():
		csrfKey := make([]byte, 32)
		_, err := rand.Read(csrfKey)
		if err != nil {
			log.Fatalf("Failed to generate CSRF key: %s", err)
		}

		r := mux.NewRouter()
		r.HandleFunc("/", homeHandler)
		r.HandleFunc("/select", selectHandler)
		r.PathPrefix("/assets/").Handler(http.StripPrefix("/assets/", http.FileServer(assetFS())))

		srv := &http.Server{
			Handler:      csrf.Protect(csrfKey, csrf.Secure(*secureCookie))(r),
			Addr:         *address,
			ReadTimeout:  30 * time.Second,
			WriteTimeout: 10 * time.Second,
			// IdleTimeout:  60 * time.Second,
		}

		log.Printf("Started webserver on %s", *address)
		err = srv.ListenAndServe()
		if err != nil {
			log.Fatal(err)
		}
	}
}
