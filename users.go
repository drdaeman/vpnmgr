package main

import (
	"errors"
	"fmt"
	"net"
	"time"

	"github.com/boltdb/bolt"
)

// Connection represents a VPN connection
type Connection struct {
	Username string
	Region   string
}

// ConnectedUsers is a database that keeps the data about VPN connections
type ConnectedUsers struct {
	db *bolt.DB
}

// OpenDatabase opens a database, given the filename
func OpenDatabase(filename string) (*ConnectedUsers, error) {
	db, err := bolt.Open(filename, 0600, &bolt.Options{Timeout: 10 * time.Second})
	if err != nil {
		return nil, err
	}
	return &ConnectedUsers{db: db}, nil
}

// Close closes the database. The ConnectedUsers instance won't be usable.
func (u *ConnectedUsers) Close() error {
	return u.db.Close()
}

// Add adds information about the connection to the database
func (u *ConnectedUsers) Add(ip net.IP, user string) error {
	return u.db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte("clients"))
		if err != nil {
			return fmt.Errorf("Failed to obtain a bucket: %s", err)
		}
		b.Put([]byte(ip.String()), []byte(user))
		return nil
	})
}

// Set sets user region preference
func (u *ConnectedUsers) Set(user string, region string) error {
	return u.db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte("region-prefs"))
		if err != nil {
			return fmt.Errorf("Failed to obtain a bucket: %s", err)
		}
		b.Put([]byte(user), []byte(region))
		return nil
	})
}

// Remove removes information about the connection from the database
func (u *ConnectedUsers) Remove(ip net.IP) error {
	return u.db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte("clients"))
		if err != nil {
			return fmt.Errorf("Failed to obtain a bucket: %s", err)
		}
		b.Delete([]byte(ip.String()))
		return nil
	})
}

// Get returns information about the connection from the database
func (u *ConnectedUsers) Get(ip net.IP) (*Connection, error) {
	var user []byte
	var region []byte
	err := u.db.View(func(tx *bolt.Tx) error {
		clients := tx.Bucket([]byte("clients"))
		if clients == nil {
			return nil
		}
		user = clients.Get([]byte(ip.String()))
		if len(user) < 1 {
			return nil
		}
		prefs := tx.Bucket([]byte("region-prefs"))
		if prefs == nil {
			return nil
		}
		region = prefs.Get(user)
		return nil
	})
	if err != nil {
		return nil, err
	}
	if len(region) < 1 {
		region = []byte("us")
	}
	return &Connection{
		Username: string(user),
		Region:   string(region),
	}, nil
}

// GetIPs returns a list of all registered connections
func (u *ConnectedUsers) GetIPs() (map[string]Connection, error) {
	result := make(map[string]Connection, 0)

	err := u.db.View(func(tx *bolt.Tx) error {
		clients := tx.Bucket([]byte("clients"))
		if clients == nil {
			return errors.New("Failed to obtain a bucket")
		}
		prefs := tx.Bucket([]byte("region-prefs"))
		c := clients.Cursor()
		for k, user := c.First(); k != nil; k, user = c.Next() {
			var region []byte
			if prefs != nil {
				region = prefs.Get(user)
			}
			if len(region) < 1 {
				region = []byte("us")
			}
			result[string(k)] = Connection{
				Username: string(user),
				Region:   string(region),
			}
		}
		return nil
	})
	return result, err
}
