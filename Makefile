#!/usr/bin/env make

.PHONY: all clean

all: vpnmgr

clean:
	rm vpnmgr bindata_assetfs.go

bindata_assetfs.go: assets
	go get github.com/jteeuwen/go-bindata/...
	go get github.com/elazarl/go-bindata-assetfs/...
	go-bindata-assetfs -modtime 0 assets

vpnmgr: bindata_assetfs.go main.go users.go
	glide install
	go build -ldflags "-s -w -linkmode external -extldflags -static" .
